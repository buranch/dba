const express = require('express');
const bodyparser = require('body-parser');


function createExpressApp(db) {
    const app = express();
    const User = db.collection("user");

    app.use(bodyparser.json());
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type , Authorization, Accept");
        next();
    });

    app.get('/api/user', (req, res) => {

        User.find({}).toArray((err, docs) => {
            if (err) {
                return res.status(500).json({
                    error: "Error While Fetching User Data"
                })
            }
            console.log(docs);
            res.status(200).json(docs);
        });
    });

    app.post('/api/user', (req, res) => {
        let formData = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            company: req.body.company,
            department: req.body.department,
            position: req.body.position,
            email: req.body.email
        }

        User.insertOne(formData, (err, result) => {
            if (err) {
                return res.status(500).json({
                    error: "Error While Inserting Form"
                })
            }
            console.log("Record Saved!");
            res.status(201).json({
                'user': result
            });
        })
    })
    return app;
}

module.exports = createExpressApp;