const mongoose = require('mongoose');
const createApp = require('./create-express-app');
var port = process.env.PORT || 3000;

DB_CONN = process.env.DB_CONN;



mongoose.connect("mongodb://MeeOpp:MeeOpp60@ds225294.mlab.com:25294/meeopp", {
    useNewUrlParser: true
}, (err, result) => {
    if (err) {
        console.log(err);
        console.log("Error while connecting to the DB");
    } else {
        console.log("Connected with the DB");
        mongoose.Promise = global.Promise;
        var db = mongoose.connection;
        createApp(db)
            .listen(port, () => {
                console.log("Server Running...", port);
            })
    }
});