const mongoose = require('mongoose');
const Schema = mongoose.Schema;


let User = new Schema({
   firstName : { type: String},
   lastName : { type: String},
   company : { type: String},
   department : { type: String},
   position : { type: String},
   email : { type: String}
});


module.exports = mongoose.model('User', User, 'user');




